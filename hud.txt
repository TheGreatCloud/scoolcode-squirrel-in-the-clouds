stage {
    
    // Helper functions:
    // Simulates default value assignment for function parameters
    function make_default(ref object, value) {
        if(object == undefined)
            object = value;
    }
    // Defaults multiple values.
    function make_defaults(list) {
        for(let i = 0; i < list.length; ++i)
            make_default(list[i][0], list[i][1]);
    }

    actor Menu {
        costume Blue("gallery:Objects/Button Blue")
        costume Pink("gallery:Objects/Button Purple")
        costume Green("gallery:Objects/Button Green")

        when started { hide(); }

        // VARIABLES
        let tooltip = "";
        let onClick = "";

        // Makes a new button and returns a reference to it.
        function makeButton(pos, onClick_, tooltip_, size_, skin_) {
            make_defaults([[ref pos, [0, 0]], [ref onClick_, ""], [ref size_, 100], [ref skin_, 0]]);
            make_default(ref tooltip_, onClick_);
            createClone(this);
            let curr = clones[clones.length - 1];
            curr.setPosition(pos[0], pos[1]);
            curr.onClick = onClick_;
            curr.tooltip = tooltip_;
            curr.size = size_;
            curr.setCostume(skin_);
            return curr;
        }

        function showButton() {
            this.say(tooltip);
            this.show();
            this.goToFront();
        }

        function showButtons(list) {
            for(let i = 0; i < list.length; ++i)
                list[i].showButton();
        }

        function hideButton() {
            this.say("");
            this.hide();
        }

        function hideButtons(list) {
            for(let i = 0; i < list.length; ++i)
                list[i].hideButton();
        }

        when clicked { stage.broadcast(onClick); }

        when pointerOver { this.size = 120; }

        when pointerOut { this.size = 100; }
    }
}